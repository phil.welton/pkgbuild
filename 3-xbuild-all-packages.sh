#!/bin/bash
#
##################################################################################################################
# Written to be used on 64 bits computers
# Author 	: 	Erik Dubois,phil
# Website 	: 	http://www.erikdubois.be
##################################################################################################################
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################
echo "#############################################################################################"
echo
echo "This builds all the existing packages"
echo "Scans the existing folders"
echo "Be sure you have the highest number for all of them"
echo
echo "#############################################################################################"

cd ~/git-me/pkgbuild/
count=0

for name in $(ls -d */); do
		count=$[count+1]
		echo
		echo "Build " $name " - nr: " $count
		echo
		cd $name
		bash build-chroot.sh
		echo "#############################################################################################"
		echo "################  "$(basename `pwd`)" done"
		echo "#############################################################################################"
		cd ..
done

		echo "#############################################################################################"
		echo "###################                 Script finished                      ####################"
		echo "#############################################################################################"
