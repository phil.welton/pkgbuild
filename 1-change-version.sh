#!/bin/bash

echo
tput setaf 2
echo "###########################"
echo "### $0 ###"
echo "###########################"
echo
echo "new version and release number"
echo "for all exsisting PKGBUILD"
echo "ascending numbers required"
tput sgr0
echo 
echo -n ".. Enter pkgver (e.g. 22.03): "
read pkgver
echo 
tput setaf 3
echo "You typed: "$pkgver
tput sgr0
echo
echo -n ".. Enter pkgrel (e.g. 01): "
read pkgrel
echo 
tput setaf 3
echo "You typed: "$pkgrel
tput sgr0
echo
echo -n "Continue ?(y/Y): "
read response
echo
if [[ "$response" == [yY] ]]; then

		count=0

		for name in $(ls -d */); do
			count=$[count+1]
			cd $name
			tput setaf 3
			echo "OK, let's go!"
			echo ...
			echo -n "Folder_$count: ";echo $name;
			sed -i "s/\(^pkgver=\).*/\1$pkgver/" PKGBUILD
			sed -i "s/\(^pkgrel=\).*/\1$pkgrel/" PKGBUILD
			echo "DONE!"
			tput sgr0
			cd ..
		done
		echo
		tput setaf 2
		echo "#### Script finished ####"
		echo 
		tput sgr0
    else
        tput setaf 2
        echo "Nothing has been changed."
        tput sgr0
fi
