#!/bin/bash

#### v.1.0 ####

echo "###############################"
echo "### Build in a Clean CHROOT ###"
echo "### make in /tmp/pbuild ###"
echo "### PKGs to repo ###"
echo "###############################"

pwd=$(basename "$PWD")
pwdpath=$(echo $PWD)

#echo "### rm old pbuild ###"
#echo "### mkdir /tmp/pbuild/ ###"
test -d "/tmp/pbuild" && rm -rf /tmp/pbuild
mkdir /tmp/pbuild
#echo"### copy pkgbuild files to pbuid ###"
cp -r $pwdpath/* /tmp/pbuild/
cd /tmp/pbuild/

echo "###################################"
echo "### Starting clean chroot build ###"
echo "###################################"
arch-nspawn $HOME/chroot/root pacman -Syu
makechrootpkg -c -r $HOME/chroot
echo "###################################"
echo "### building Done! ###"
echo "###################################"
gpg --detach-sign --default-key phil.welton $pwd*pkg.tar.zst
mv $pwd*pkg.tar.zst /Projekte/phil_repo/x86_64/
mv $pwd*.sig /Projekte/phil_repo/x86_64/
