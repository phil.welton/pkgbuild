#!/bin/bash

cd /Projekte/pkgbuild
bash 1-change-version.sh
bash 2-copy-build-file-to-all-folders.sh
bash 3-xbuild-all-packages.sh

echo "####      All In One...      ####"
echo "####      Script finished !   ####"
